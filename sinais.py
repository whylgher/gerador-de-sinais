import time
from datetime import datetime, timedelta
from dateutil import tz
from take_candles import goal, dias, time_candle, taxa_acertividade


def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x + 0).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))


def timestamp_calculate(dia):
    time_dias = timedelta(days=dia).total_seconds()
    data = time.time() + time_dias
    dia_conv = timestamp_converter(data)
    return data, dia_conv


def sep_for_days():
    arquivo = open('sinais/trantando_sinais.txt', 'w')
    arquivo.close()
    arquivo = open('sinais/trantando_sinais.txt', 'r')
    conteudo = arquivo.readlines()

    listando_sinais = open('sinais/sinais_totais.txt', 'r')
    lista_sinal = listando_sinais.readlines()
    listando_sinais.close()

    dias_x = int(1440 / time_candle)
    tam_list = int(len(lista_sinal))

    for candle_goal in goal:
        for i in range(dias_x):
            for x in range(i, tam_list, dias_x):
                c = lista_sinal[x]
                c = c.split()

                if candle_goal == c[8]:
                    conteudo.append(f'{c[3]} {c[5]} {c[6]}  {c[8]} {c[9]} ')

            conteudo.append('\n')

    arquivo = open('sinais/trantando_sinais.txt', 'w')
    arquivo.writelines(conteudo)
    arquivo.close()


def finish_signals():
    arquivo = open('sinais.txt', 'w')
    arquivo.close()

    arquivo = open('sinais/trantando_sinais.txt', 'r')
    lista_sinal = arquivo.readlines()
    tam_list = int(len(lista_sinal))
    arquivo.close()

    arquivo = open('sinais/trantando_sinais.txt', 'r')
    singnal_finish = open('sinais.txt', 'r')
    conteudo = singnal_finish.readlines()

    for candle_goal in goal:
        for y in range(int(tam_list / len(goal))):
            try:
                c = arquivo.readline()
                c = c.split()

                green = c.count('GREEN')
                red = c.count('RED')
                trat_green = "%.2f" % ((green / dias) * 100)
                trat_red = "%.2f" % ((red / dias) * 100)
                tmdata, dia_conv = timestamp_calculate(1)
                tmdata2, days_todays = timestamp_calculate(0)
                day_today = days_todays.split()
                day_today = day_today[0]
                dias_conv = dia_conv
                dias_conv = dias_conv.split()
                dias_conv = dias_conv[0]
                hours_minutes = c[2]
                hours_minutes = str(hours_minutes.split())
                dia_sinal = c[1]
                # print(c)

                # print(day_today[8:], dia_sinal[8:], int(dias_conv[8:]) - 1)
                print(f'CALL:{trat_green}% || PUT: {trat_red}%  -  {time_candle}, {c[4]},{day_today} {c[2]} {c[3]}')
                if float(trat_green) >= taxa_acertividade and int(dia_sinal[8:]) < (int(dias_conv[8:]) - 1):
                    conteudo.append(f'{time_candle},{candle_goal},{day_today} {c[2]},CALL\n')

                if float(trat_red) >= taxa_acertividade and int(dia_sinal[8:]) < (int(dias_conv[8:]) - 1):
                    conteudo.append(f'{time_candle},{candle_goal},{day_today} {c[2]},PUT\n')

                if float(trat_green) >= taxa_acertividade and int(dia_sinal[8:]) >= (int(dias_conv[8:]) - 1):
                    conteudo.append(f'{time_candle},{candle_goal},{dias_conv} {c[2]},CALL\n')

                if float(trat_red) >= taxa_acertividade and int(dia_sinal[8:]) >= (int(dias_conv[8:]) - 1):
                    conteudo.append(f'{time_candle},{candle_goal},{dias_conv} {c[2]},PUT\n')

            except IndexError:
                pass

    singnal_finish = open('sinais.txt', 'w')
    singnal_finish.writelines(conteudo)
    singnal_finish.close()
    arquivo.close()

    print('Finished')
