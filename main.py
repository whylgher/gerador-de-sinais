from take_candles import take_candles
from sinais import sep_for_days, finish_signals


def __start__():
    print()
    print('Take Candles')
    take_candles()
    print('Cut by day')
    sep_for_days()
    print('Finish')
    finish_signals()


if __name__ == '__main__':
    print('Iniciando')
    __start__()
