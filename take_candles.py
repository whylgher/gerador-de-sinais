import time
from datetime import datetime, timedelta
from iqoptionapi.stable_api import IQ_Option
from dateutil import tz

Iq = IQ_Option("7bitsmarket@gmail.com", "lionSEX497")
Iq.connect()
goal = 'EURUSD', 'AUDUSD', 'EURGBP', 'GBPJPY', 'AUDCAD', 'EURJPY'
# goal = 'EURUSD', 'AUDUSD', 'EURGBP', 'GBPJPY', 'AUDCAD', 'EURJPY', 'GBPUSD', 'USDJPY', 'USDCAD', 'AUDJPY', 'USDCHF', 'AUDCHF', 'GBPCAD', 'GBPNZD', 'EURNZD', 'GBPCHF', 'USDNOK', 'AUDNZD', 'GBPAUD'
print("get candles")

taxa_acertividade = 75
dias = 14
time_candle = 5
time_candles = time_candle * 60
dia = (1440 * dias) / time_candle


def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x + 0).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))


if time_candle >= 60:
    time_frame = f'H{int(time_candle / 60)}'
    qt_candle = int(1440 / time_candle)
else:
    time_frame = f'M{time_candle}'
    qt_candle = int(60 / time_candle * 24)

time_now = timestamp_converter(time.time())
print(time_now)


def timestamp_calculate(dia, goal):
    time_dias = timedelta(days=dia).total_seconds()
    data = time.time() + time_dias
    dia_conv = timestamp_converter(data)
    id_candle = Iq.get_candles(goal, time_candles, int(qt_candle), data)
    return id_candle, dia_conv


def take_candles():
    arquivo = open('sinais/sinais_totais.txt', 'w')
    arquivo.close()
    arquivo = open('sinais/sinais_totais.txt', 'r')
    conteudo = arquivo.readlines()
    x = 0
    v = 1

    for candle_goal in goal:
        n = 1
        qt_velas = int(dia / dias)
        for dia_velas in range(dias):
            id_candle, dia_conv = timestamp_calculate(x, candle_goal)
            for candle in id_candle:

                id = candle['id']
                if candle["close"] > candle["open"]:
                    conteudo.append(
                        f"id: {id} vela: GREEN data: {timestamp_converter(candle['from'])} GOAL: {candle_goal} {candle['from']}\n")
                    print(f"id: {id} vela: GREEN data: {timestamp_converter(candle['from'])} GOAL: {candle_goal} \n")
                if candle["close"] < candle["open"]:
                    conteudo.append(
                        f"id: {id} vela: RED data: {timestamp_converter(candle['from'])} GOAL: {candle_goal} {candle['from']}\n")
                    print(f"id: {id} vela: RED data: {timestamp_converter(candle['from'])} GOAL: {candle_goal} \n")
                if candle["close"] == candle["open"]:
                    conteudo.append(
                        f"id: {id} vela: GRAY data: {timestamp_converter(candle['from'])} GOAL: {candle_goal} {candle['from']}\n")

                if v >= qt_velas:
                    v = 1
                    if n == dias:
                        n = 1
                    else:
                        n = n + 1
                else:
                    v = v + 1
            x = x - 1
        v = 1
        x = 0
    arquivo = open('sinais/sinais_totais.txt', 'w')
    arquivo.writelines(conteudo)
    arquivo.close()
