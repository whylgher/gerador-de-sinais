# Demonstração da função Timedelta

from datetime import datetime, timedelta

# Usando a hora atual
ini_time_for_now = datetime.now()

# impressão data_inicial
print("initial_date", str(ini_time_for_now))

# Calculando datas futuras
future_date_after_2yrs = ini_time_for_now + timedelta(days=730)
# Calculando datas passadas
future_date_back = ini_time_for_now + timedelta(
    days=50,
    seconds = 27,
    microseconds = 10,
    milliseconds = 29000,
    minutes = 5,
    hours = 8,
    weeks = 2)

# printing calculated future_dates
print('future_date_after_2yrs:', str(future_date_after_2yrs))
print('future_date_back:', str(future_date_back))